<?php

//include_once 'admin/customizer.php';
//include_once 'admin/sidebars.php';

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('custom-logo');

register_nav_menu('header-primary', 'Header Menu');

//add_action('init', 'init_func');
function init_func ()
{
	flush_rewrite_rules();
}

add_action('wp_enqueue_scripts', 'enqueue_styles_scripts_func');
function enqueue_styles_scripts_func ()
{
	wp_enqueue_style('theme-css', get_stylesheet_directory_uri() . '/css/styles.css', array(), '0.0.1');

	wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/modernizr.js', array(), '0.0.1', FALSE);

	//$google_fonts_url = add_query_arg('family', 'Roboto:400,400i,700,700i', '//fonts.googleapis.com/css');
	//wp_enqueue_style('googlefonts', $google_fonts_url, array(), NULL);

	//wp_enqueue_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), NULL);
}

function get_all_post_meta ($post_id = NULL)
{
	if (empty($post_id)) {
		global $post;
		$post_id = $post->ID;
	}

	$post_meta = get_post_meta($post_id);

	if (is_array($post_meta) && count($post_meta) > 0) {
		foreach ($post_meta as $key => &$value) {
			if (is_array($value) && count($value) === 1) {
				$value = $value[0];
			}
			if (is_serialized($value)) {
				$value = unserialize($value);
			}
		}
	}

	return $post_meta;
}

add_action('get_header', 'get_header_func');
function get_header_func ()
{
	global $post, $post_meta;

	$post_meta = (object) get_all_post_meta($post->ID);
}

add_filter('body_class', 'add_slug_to_body_class');
function add_slug_to_body_class ($classes)
{
	global $post;
	if (isset($post)) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}

	return $classes;
}

// Allow SVG
//add_filter('upload_mimes', 'upload_mimes_func');
function upload_mimes_func ($mimes)
{
	$mimes['svg']  = 'image/svg+xml';
	$mimes['svgz'] = 'image/svg+xml';

	return $mimes;
}

//add_filter('wp_check_filetype_and_ext', 'wp_check_filetype_and_ext_func', 10, 4);
function wp_check_filetype_and_ext_func ($data, $file, $filename, $mimes)
{
	global $wp_version;
	if (version_compare($wp_version, '4.7.1', '<')) {
		return $data;
	}

	$filetype = wp_check_filetype($filename, $mimes);

	return [
		'ext'             => $filetype['ext'],
		'type'            => $filetype['type'],
		'proper_filename' => $data['proper_filename']
	];
}

remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('template_redirect', 'rest_output_link_header', 11);

remove_action('wp_head', 'rsd_link'); // remove rel="EditURI" link. USEFUL TO REMOVE THIS FOR SECURITY.
remove_action('wp_head', 'wlwmanifest_link'); // remove the link rel="wlwmanifest"
remove_action('wp_head', 'wp_generator'); // remove the meta generator WP version tag
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10); // remove both link rel="prev" and link rel="next"
remove_action('wp_head', 'feed_links', 2); // remove the link rel="alternate" feed links for regular posts fee and comments feed
remove_action('wp_head', 'feed_links_extra', 3); // Remove links to the extra feeds (e.g. category feeds)