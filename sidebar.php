<?php
/**
 * The Sidebar containing the main widget area
 */
?>

<?php if (is_active_sidebar('sidebar')) : ?>
	<aside id="secondary" class="widget-area" role="complementary">
		<?php dynamic_sidebar('sidebar'); ?>
	</aside>
<?php endif; ?>