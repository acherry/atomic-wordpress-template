<?php get_header(); ?>

<?php global $post, $post_meta; ?>

	<div id="content" class="site-content">

		<div id="primary" class="content-area">

			<main id="main" class="site-main" role="main">

				<?php

				// Start the Loop.
				while (have_posts()) : the_post();

					// Include the page content template.
					get_template_part('template-parts/content', $post->post_type);

					// If comments are open or we have at least one comment, load up the comment template.
					if (comments_open() || get_comments_number()) {
						comments_template();
					}

				endwhile;

				?>

			</main>

		</div>

	</div>

<?php get_footer(); ?>