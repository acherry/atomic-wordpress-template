<!DOCTYPE html>
<!--[if IE 6]>
<html class="cwt-reset ie lt-ie11 lt-ie10 lt-ie9 lt-ie8 lt-ie7 ie6 no-js"><![endif]-->
<!--[if IE 7]>
<html class="cwt-reset ie lt-ie11 lt-ie10 lt-ie9 lt-ie8 ie7 gt-ie6 no-js"><![endif]-->
<!--[if IE 8]>
<html class="cwt-reset ie lt-ie11 lt-ie10 lt-ie9 ie8 gt-ie7 gt-ie6 no-js"><![endif]-->
<!--[if IE 9]>
<html class="cwt-reset ie lt-ie11 lt-ie10 ie9 gt-ie8 gt-ie7 gt-ie6 no-js"><![endif]-->
<!--[if !IE]> -->
<html <?php language_attributes(); ?> class="cwt-reset no-ie no-js">
<!-- <![endif]-->
<head profile="http://www.w3.org/2005/10/profile">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="robots" content="index,follow" />
	<meta name="KeyWords" content="" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<?php global $post, $post_meta;

$wp_nav_settings_header_primary = array(
	'theme_location' => 'header-primary',
	'menu_class'     => 'main-nav-menu header-primary-menu',
	'container'      => FALSE,
	'echo'           => FALSE
);

$wp_nav_header_primary = wp_nav_menu($wp_nav_settings_header_primary);

?>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<a class="screen-reader-text skip-link assistive-text" href="#content">Skip to content</a>

	<header id="masthead" class="site-header" role="banner">
		<div>

			<div class="site-branding">
				<div class="site-logo"><a href="/"><img src="<?php echo wp_get_attachment_url(get_theme_mod('custom_logo')); ?>" /></a></div>
			</div>

			<div class="site-header-menus">
				<div id="site-header-primary-menu" class="site-header-primary-menu">
					<nav class="main-navigation" role="navigation"><?php echo $wp_nav_header_primary; ?></nav>
				</div>
			</div>

		</div>
	</header>