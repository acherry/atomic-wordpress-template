<?php

add_action('widgets_init', 'widgets_init_func');
function widgets_init_func ()
{
	register_sidebar(array(
		                 'name'          => 'Sidebar',
		                 'id'            => 'sidebar',
		                 'before_widget' => '',
		                 'after_widget'  => '',
		                 'before_title'  => '',
		                 'after_title'   => '',
	                 ));
}