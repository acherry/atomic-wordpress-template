<?php

add_action('customize_register', 'customize_register_func');
function customize_register_func ($wp_customize)
{
	if (!class_exists('WP_Customize_Control')) {
		return NULL;
	}

	/* Header */

	$wp_customize->add_section('header', array(
		'title' => 'Header'
	));

	$wp_customize->add_setting('header_logo', array(
		'default'   => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'header_logo_ctrl', array(
		'label'    => 'Choose Full Size Logo',
		'section'  => 'header',
		'settings' => 'header_logo'
	)));

	/* Footer */

	$wp_customize->add_section('footer', array(
		'title' => 'Footer'
	));

	$wp_customize->add_setting('footer_text', array(
		'default'   => '',
		'transport' => 'refresh',
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_text_ctrl', array(
		'label'    => 'Footer',
		'section'  => 'footer',
		'settings' => 'footer_text',
		'type'     => 'textarea',
	)));

}