<?php get_header(); ?>

	<div id="content" class="site-content">

		<div id="primary" class="content-area">

			<main id="main" class="site-main" role="main">

				<article class="error-404 not-found">
					<header class="entry-header">
						<h1 class="entry-title">Not Found</h1>
					</header>

					<div class="entry-content">
						404
					</div>
				</article>

			</main>

		</div>

	</div>

<?php get_footer(); ?>