<?php get_header(); ?>

<?php global $post, $post_meta; ?>

	<div id="content" class="site-content">

		<div id="primary" class="content-area">

			<main id="main" class="site-main" role="main">

				<?php
				if (have_posts()) :
					while (have_posts()) : the_post();
						get_template_part('template-parts/content', (get_post_format() ? get_post_format() : $post->post_type));
					endwhile;
				else :?>
					<article>
						<div class="entry-content">
							Nothing has been posted yet. Check back soon!
						</div>
					</article>
					<?php
				endif;

				if ($GLOBALS['wp_query']->max_num_pages > 1) {
					?>
					<div class="read-more-articles">
						<?php
						// Previous/next page navigation.
						the_posts_pagination(array(
							                     'prev_text' => '&laquo; Previous',
							                     'next_text' => 'Next &raquo;'
						                     ));
						?>
					</div>
					<?php
				}
				?>

			</main>

		</div>

	</div>

<?php get_footer(); ?>