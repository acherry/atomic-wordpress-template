<?php
/**
 * The default template for displaying content
 * Used for both single and index/archive/search.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
		global $more;
		if (!$more && is_home()) :
			the_title('<h3 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>');
		endif;
		?>
	</header>


	<div class="entry-content">

		<?php

		/* translators: %s: Name of current post */
		the_content(sprintf('Continue reading %s', the_title('<span class="screen-reader-text">', '</span>', FALSE)));

		wp_link_pages(array(
			              'before'      => '<div class="page-links"><span class="page-links-title">Pages:</span>',
			              'after'       => '</div>',
			              'link_before' => '<span>',
			              'link_after'  => '</span>',
			              'pagelink'    => '<span class="screen-reader-text">Page </span>%',
			              'separator'   => '<span class="screen-reader-text">, </span>',
		              ));
		?>

	</div>

	<?php if (is_user_logged_in()) : ?>
		<div class="entry-footer">
			<?php // edit_post_link('Edit', '<span class="edit-link">', '</span>'); ?>
		</div>
	<?php endif; ?>

</article><!-- #post-## -->